#Create sum function
def sum(x,y):
    sum = x + y
    #Check whether sum in range(15, 20)
    if sum in range(15, 20):
        return 20
    else:
        return sum

#Print the output
print(sum(10, 6))
print(sum(10, 2))
print(sum(10, 12))