import datetime
#Assign the current date & time
now = datetime.datetime.now()
#Print the current date & time
print("Current date and time : ")
print(now.strftime("%Y-%m-%d %H:%M:%S"))